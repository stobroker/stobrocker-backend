class Index {

  get server() {
    return {
      port: process.env.PORT || 5919,
      secret: process.env.SECRET || 'fkagdnbfgbxsgfbvrbwrrutb4vf35fdc0paz',
      expiresIn: process.env.EXPIRES_IN || 14 * 24 * 60 * 60,
      refreshTokenSecret: process.env.REFRESH_TOKEN_SECRET || 'sgbfcaxcmmutbrneva4m986bngl4aq3g',
      refreshTokenExpiresIn: process.env.REFRESH_TOKEN_EXPIRES_IN || 365 * 24 * 60 * 60,
      appURL: process.env.APP_URL || 'http://localhost:5919'
    };
  }

  get postgres() {
    return {
      username: process.env.PG_USERNAME || 'postgres',
      password: process.env.PG_PASSWORD || 'postgres',
      host: process.env.PG_HOST || 'localhost',
      database: process.env.PG_DATABASE || 'stobrocker',
      port: process.env.PG_PORT || 5432
    };
  }

  get loggingLevel() {
    return process.env.LOGGING_LEVEL || 'debug';
  }

  get longStackTraces() {
    return true;
  }

  get nodeEnv() {
    return process.env.NODE_ENV || 'test'; // local test development production
  }

  get uploadFileLimits() {
    return {
      files: 100,
      fileSize: 100000000
    }
  }
}

module.exports = new Index();
