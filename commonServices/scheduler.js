const errorMessages = require('./../commonServices/errorMessages');
const AppError = require('./../commonServices/error');
const db = require('../db');
const schedule = require('node-schedule');
const {Op} = require('sequelize');

module.exports = class scheduler {

  static async startIssue() {
    try {
      await schedule.scheduleJob('*/1 * * * *', await this.startStatusIssue);
    } catch (err) {
      if (err instanceof AppError) throw err;
      throw new AppError({err: err});
    }
  }

  static async completeIssue() {
    try {
      await schedule.scheduleJob('*/1 * * * *', await this.completeStausIssue);
    } catch (err) {
      if (err instanceof AppError) throw err;
      throw new AppError({err: err});
    }
  }

  static async startStatusIssue() {
    try {
      const models = db.getModels();
      const dateNow = new Date(Date.now());
      let issues = await models.Issue.findAll({
        where: {
          status: 'PENDING',
          startDate: {[Op.lt]: dateNow},
          endDate: {[Op.gt]: dateNow}
        },
      });
      for (let i = 0; i < issues.length; i++) {
        issues[i].set('status', 'LAUNCHED');
        await issues[i].save();
      }

    } catch (err) {
      if (err instanceof AppError) throw err;
      throw new AppError({err: err});
    }
  }

  static async completeStausIssue() {
    try {
      const models = db.getModels();
      const dateNow = new Date(Date.now());
      let issues = await models.Issue.findAll({
        where: {
          status: 'LAUNCHED',
          endDate: {[Op.lte]: dateNow}
        },
      });
      for (let i = 0; i < issues.length; i++) {
        issues[i].set('status', 'COMPLETED');
        await issues[i].save();
      }
    } catch (err) {
      if (err instanceof AppError) throw err;
      throw new AppError({err: err});
    }
  }

};
