const winston = require('winston');
const config = require('../config');

let Telegraf = null;
let bot = null;

if (config.botToken) {
  Telegraf = require('telegraf');
  bot = new Telegraf(config.botToken);
}


const toJson = obj => JSON.stringify(obj, null, 4);

const sendErrorToBot = (infoForBot, context, timestamp) => {
  infoForBot = infoForBot.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');

  const chatId = config.nodeEnv !== 'production' ? 285907377 : (context.noisy ? -1001459094047 : -1001284467481);

  const extra = {
    parse_mode: 'HTML',
    //disable_notification: !context.level.includes('error')
  };


  if (infoForBot.length < 4000) {
    bot.telegram.sendMessage(chatId, config.nodeEnv.toUpperCase() + ' <pre>' + infoForBot + '</pre>', extra)
    .catch(err => console.log(timestamp + ' | ' + context.level + ' | error from log bot: ', err));
  } else {
    const data = Buffer.from(infoForBot, 'utf8');
    const fileName = 'Error.txt';

    extra.caption = config.nodeEnv.toUpperCase() + ' <pre>' + infoForBot.slice(0, 800) + ' ... </pre>';

    bot.telegram.sendDocument(chatId, {source: data, filename: fileName}, extra)
    .catch(err => console.log(timestamp + ' | ' + context.level + ' | error from log bot: ', err));
  }
};

const consoleFormat = winston.format.combine(
  winston.format.colorize(),
  winston.format.timestamp(),
  winston.format.splat(),
  winston.format.errors({stack: true}),
  winston.format.printf(info => {
      const timestamp = (typeof info.timestamp.toISOString === 'function') ? info.timestamp.toISOString() : info.timestamp;

      const formattedInfo = timestamp
        + ' | ' + info.level + ' | '
        + (info.message
          ? info.message//info.message.toString().split('{').join('\n{')
          : 'no message ')
        + (info.stack
          ? (' | ' + info.stack)
          : '')
        + ((info.level.includes('error') && info.stackOfParentErr)
          ? ('\n' + timestamp + ' | stack of parent ' + info.level + ' | ' + info.stackOfParentErr)
          : '')
        + ((info.level.includes('error') || info.level.includes('warn'))
          ? ('\n' + timestamp + ' | ' + info.level + ' details in JSON | ' + toJson(info))
          : '');

      if (bot && (info.level.includes('error') || (info.level.includes('warn') && config.nodeEnv !== 'production'))) {
        sendErrorToBot(formattedInfo, info, timestamp)
      }

      return formattedInfo
    }
  )
);

const transports = [new winston.transports.Console()];
if (['development', 'production'].includes(config.nodeEnv)) transports.push(new winston.transports.File({
  filename: '/logs/combined.log',
  maxsize: 10000000, // 10 mb
  //zippedArchive: true, //
  maxFiles: 9900, // 99 gb
  tailable: true
}));

const logger = winston.createLogger({
  level: config.loggingLevel || 'info',//error warn info verbose debug silly
  transports,
  format: consoleFormat
});


logger.formatRequestInfo = function (req, preStr = 'REQUEST') {
  let info = preStr + ' ' + req.method + ' ' + req.originalUrl + ' "req.timestamp": ' + req.timestamp + ' "req.user": ' + toJson(req.user);
  if (req.body
    && Object.keys(req.body).length !== 0
    && !req.originalUrl.includes('auth')) info += ' "req.body": ' + toJson(req.body);

  return info;
};

logger.formatResponseInfo = function (req, status, response) {
  let info = logger.formatRequestInfo(req, 'RESPONSE');
  if (!req.originalUrl.includes('auth')) info += ' "res.status().json()": ' + status + ' ' + toJson(response);

  return info;
};

module.exports = logger;