const errorMessages = require('./../../../commonServices/errorMessages');
const AppError = require('./../../../commonServices/error');
const respond = require('../middlewares/respond');

const generateMessage = require('../core/auth/generateMessage');
const verify = require('../core/auth/verify');

async function auth() {

  const app = this.app;

  app.route('/api/v1/auth/message')
      .post((req, res) => {
        if (!req.body.ethereumAddress)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, generateMessage(req.body.ethereumAddress));
      });

  app.route('/api/v1/auth/verify')
      .post((req, res) => {
        if (!req.body.ethereumAddress || !req.body.signature)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, verify(req.body.ethereumAddress, req.body.signature));
      });

  app.route('/api/v1/test/ping')
      .get((req, res) => {
        return res.status(400).json({status: 'ok3'});
      });

}

module.exports = auth;
