const respond = require('../middlewares/respond');
const reqAuth = require('./../middlewares/reqAuth');
const errorMessages = require('./../../../commonServices/errorMessages');
const AppError = require('./../../../commonServices/error');

const getVerifiedCredentialDocuments = require('../core/user/getVerifiedCredentialDocuments');
const deleteShareVcRequest = require('../core/user/deleteShareVcRequest');
const getProposal = require('../core/user/getProposal');

async function userController() {

  const app = this.app;

  app.route('/api/v1/user/verifiedCredentialDocuments')
      .all(reqAuth)
      .get((req, res) => {
        respond(res, 200, getVerifiedCredentialDocuments(req.user));
      });

  app.route('/api/v1/user/shareVcRequest')
      .all(reqAuth)
      .delete((req, res) => {
        if (!req.body.shareVcRequestId)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, deleteShareVcRequest(req.user, req.body.shareVcRequestId));
      });

  app.route('/api/v1/user/proposal')
      .all(reqAuth)
      .get((req, res) => {
        if (!req.query.proposalId)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, getProposal(req.user, req.query.proposalId));
      });

}

module.exports = userController;
