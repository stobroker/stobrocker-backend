const errorMessages = require('./../../../commonServices/errorMessages');
const AppError = require('./../../../commonServices/error');

const respond = require('../middlewares/respond');
const reqAuth = require('./../middlewares/reqAuth');
const reqVerifier = require('./../middlewares/reqVerifier');

const getSignedProposals = require('../core/verifier/getSignedProposals');
const verifierProposal = require('../core/verifier/verifierProposal');
const createShareVcRequest = require('../core/user/createShareVcRequest');
const getShareVcRequests = require('../core/user/getShareVcRequests');

async function verifierController() {

  const app = this.app;

  app.route('/api/v1/verifier/signedProposals')
    .all(reqAuth, reqVerifier)
    .get((req, res) => {
      respond(res, 200, getSignedProposals(req.user));
    });

  app.route('/api/v1/verifier/verifierProposal')
    .all(reqAuth, reqVerifier)
    .post((req, res) => {
      if (!req.body.proposalId)
        return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
      respond(res, 200, verifierProposal(req.user, req.body));
    });

  app.route('/api/v1/verifier/shareVcRequest')
      .all(reqAuth, reqVerifier)
      .post((req, res) => {
        if (!req.body.proposalId || !req.body.credentialId)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, createShareVcRequest(req.user, req.body));
      });

  app.route('/api/v1/verifier/shareVcRequests')
      .all(reqAuth, reqVerifier)
      .get((req, res) => {
        if (!req.query.proposalId)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, getShareVcRequests(req.user, req.query.proposalId));
      })


}

module.exports = verifierController;
