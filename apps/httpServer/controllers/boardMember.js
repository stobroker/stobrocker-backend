const errorMessages = require('./../../../commonServices/errorMessages');
const AppError = require('./../../../commonServices/error');
const respond = require('../middlewares/respond');
const reqAuth = require('./../middlewares/reqAuth');
const reqBoardMember = require('./../middlewares/reqBoardMember');

const getProposals = require('../core/boardMember/getProposals');
const createIssue = require('../core/boardMember/createIssue');
const voting = require('../core/boardMember/voting');
const resultVoting = require('../core/boardMember/resultVoting');
const signedProposal = require('../core/boardMember/signedProposal');
const createShareVcRequest = require('../core/user/createShareVcRequest');
const getShareVcRequests = require('../core/user/getShareVcRequests');
const verifiedCredentialDocument = require('../core/boardMember/verifiedCredentialDocument');
const addSignature = require('../core/boardMember/addSignature');
const createOrUpdateVC = require('../core/user/createOrUpdateVC');
const addVCDocumentsInProposal = require('../core/boardMember/addVCDocumentsInProposal');
const getMyShareVcRequests = require('../core/user/getMyShareVcRequests');
const putShareVcRequest = require('../core/user/putShareVcRequest');

async function boardMemberController() {

  const app = this.app;

  app.route('/api/v1/boardMember/proposals')
      .all(reqAuth, reqBoardMember)
      .get((req, res) => {
        respond(res, 200, getProposals(req.user));
      });

  app.route('/api/v1/boardMember/verifiedCredentialDocument')
      .all(reqAuth, reqBoardMember)
      .put((req, res) => {
        if (!req.body.proposalId || !req.body.credentialId)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, verifiedCredentialDocument(req.user, req.body));
      });

  app.route('/api/v1/boardMember/issue')
      .all(reqAuth, reqBoardMember)
      .post((req, res) => {
        if (!req.body.proposalId ||
            !req.body.title ||
            !req.body.description ||
            !req.body.startDate ||
            !req.body.endDate ||
            !req.body.supportPercentNeed ||
            !req.body.minimalApprovalPercentNeed)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 201, createIssue(req.user, req.body));
      });

  app.route('/api/v1/boardMember/voting')
      .all(reqAuth, reqBoardMember)
      .get((req, res) => {
        if (!req.query.issueId)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, resultVoting(req.user, req.query.issueId));
      })
      .post((req, res) => {
        if (!req.body.issueId ||
            !(req.body.voice === 'YES' || req.body.voice === 'NO' || req.body.voice === 'ABSTAIN'))
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, voting(req.user, req.body));
      });

  app.route('/api/v1/boardMember/signedProposal')
      .all(reqAuth, reqBoardMember)
      .post((req, res) => {
        if (!req.body.issueId || !req.body.proposalId)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, signedProposal(req.user, req.body));
      });

  app.route('/api/v1/boardMember/shareVcRequest')
      .all(reqAuth, reqBoardMember)
      .post((req, res) => {
        if (!req.body.proposalId || !req.body.credentialId)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, createShareVcRequest(req.user, req.body));
      });

  app.route('/api/v1/boardMember/shareVcRequests')
      .all(reqAuth, reqBoardMember)
      .get((req, res) => {
        if (!req.query.proposalId)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, getShareVcRequests(req.user, req.query.proposalId));
      })

  app.route('/api/v1/boardMember/signature')
      .all(reqAuth, reqBoardMember)
      .post((req, res) => {
        if (!req.body.proposalId || !req.body.signature)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, addSignature(req.user, req.body));
      });

  app.route('/api/v1/boardMember/createOrUpdateVC')
      .all(reqAuth, reqBoardMember)
      .post((req, res) => {
        if (!req.body.documentsJSON)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, createOrUpdateVC(req.user, req.body));
      });

  app.route('/api/v1/boardMember/proposal/VCDocuments')
      .all(reqAuth, reqBoardMember)
      .post((req, res) => {
        if (!req.body.proposalId || !req.body.providedCredentials || !req.body.providedCredentials.length)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, addVCDocumentsInProposal(req.user, req.body));
      });

  app.route('/api/v1/boardMember/shareVcRequest')
      .all(reqAuth, reqBoardMember)
      .get((req, res) => {
        respond(res, 200, getMyShareVcRequests(req.user));
      })
      .put((req, res) => {
        if (!req.body.responseToken || !req.body.shareVcRequestId)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, putShareVcRequest(req.user, req.body));
      });

}

module.exports = boardMemberController;
