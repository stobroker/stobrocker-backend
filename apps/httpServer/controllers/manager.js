const errorMessages = require('./../../../commonServices/errorMessages');
const AppError = require('./../../../commonServices/error');

const respond = require('../middlewares/respond');
const reqAuth = require('./../middlewares/reqAuth');
const reqManager = require('./../middlewares/reqManager');
const fileUpload = require('./../middlewares/fileUpload');

const createProposal = require('../core/manager/createProposal');
const getSignedProposals = require('../core/manager/getSignedProposals');
const createOrUpdateVC = require('../core/user/createOrUpdateVC');
const getMyShareVcRequests = require('../core/user/getMyShareVcRequests');
const putShareVcRequest = require('../core/user/putShareVcRequest');
const resultVoting = require('../core/manager/resultVoting');
const sendContract = require('../core/manager/sendContract');


async function managerController() {

  const app = this.app;

  app.route('/api/v1/manager/proposal')
      .all(reqAuth, reqManager)
      .post(fileUpload('manager'), (req, res) => {
        let authDoc, additionalDoc;
        for (let i = 0; i < req.files.length; i++) {
          if (req.files[i].fieldname === 'authDoc') {
            authDoc = req.files[i];
            break;
          }
        }
        for (let i = 0; i < req.files.length; i++) {
          if (req.files[i].fieldname === 'additionalDoc') {
            additionalDoc = req.files[i];
            break;
          }
        }
        if (!req.body.type
            || (req.body.type !== 'PURCHASE_CONTRACT_PROXY' || req.body.type !== 'PURCHASE_CONTRACT_PROXY' || req.body.type !== 'PURCHASE_CONTRACT_PROXY')
            || !req.body.validity
            || !req.body.description
            || !authDoc
            || !additionalDoc
            || !req.body.authDocSign
            || !req.body.additionalDocSign
            || !req.body.providedCredentials
            || !req.body.providedCredentials.length
        )
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 201, createProposal(req.user, req.body, authDoc, additionalDoc));
      });

  app.route('/api/v1/manager/signedProposals')
      .all(reqAuth, reqManager)
      .get((req, res) => {
        respond(res, 200, getSignedProposals(req.user));
      });

  app.route('/api/v1/manager/createOrUpdateVC')
      .all(reqAuth, reqManager)
      .post((req, res) => {
        if (!req.body.documentsJSON)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, createOrUpdateVC(req.user, req.body));
      });

  app.route('/api/v1/manager/shareVcRequest')
      .all(reqAuth, reqManager)
      .get((req, res) => {
        respond(res, 200, getMyShareVcRequests(req.user));
      })
      .put((req, res) => {
        if (!req.body.responseToken || !req.body.shareVcRequestId)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, putShareVcRequest(req.user, req.body));
      });

  app.route('/api/v1/manager/voting')
      .all(reqAuth, reqManager)
      .get((req, res) => {
        if (!req.query.issueId)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, resultVoting(req.user, req.query.issueId));
      });

  app.route('/api/v1/manager/sendContract')
      .all(reqAuth, reqManager)
      .post((req, res) => {
        if (!req.body.proposalId || !req.body.issueId || !req.body.contractSignature)
          return res.status(400).json(new AppError({status: 400, message: errorMessages.BAD_DATA}));
        respond(res, 200, sendContract(req.user, req.body));
      });

  app.route('/api/v1/verifier/signedProposals')
      .all(reqAuth, reqVerifier)
      .get((req, res) => {
        respond(res, 200, getSignedProposals(req.user));
      });

}

module.exports = managerController;
