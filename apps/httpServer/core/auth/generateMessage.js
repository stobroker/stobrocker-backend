const AppError = require('./../../../../commonServices/error');
const errorMessages = require('./../../../../commonServices/errorMessages');
const db = require('../../../../db');
const crypto = require('crypto');
const {Op} = require('sequelize');

async function generateMessage(ethereumAddress) {
    try {
        const models = db.getModels();
        let user = {}, result = {};
        console.log('ethereumAddress',ethereumAddress)
        user = await models.Verifier.findOne({where: {ethereumAddress: {[Op.iLike]: ethereumAddress}}});
        if (!user) user = await models.BoardMember.findOne({where: {ethereumAddress: {[Op.iLike]: ethereumAddress}}});
        if (!user) user = await models.Manager.findOne({where: {ethereumAddress: {[Op.iLike]: ethereumAddress}}});
        if (!user) throw new AppError({status: 400, message: errorMessages.USER_NOT_FOUND});

        const message = crypto.randomBytes(16).toString('hex');
        await models.UserMessage.create({
            userId: user.id,
            userRole: user.role,
            message: message
        });
        result.message = message;
        result.userRole = user.role;
        return result;
    } catch (err) {
        if (err instanceof AppError) throw err;
        throw new AppError({err: err});
    }
}

module.exports = generateMessage;
