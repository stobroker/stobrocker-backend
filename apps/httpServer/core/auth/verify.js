const AppError = require('./../../../../commonServices/error');
const errorMessages = require('./../../../../commonServices/errorMessages');
const db = require('../../../../db');
const config = require('../../../../config');
const jwt = require('jsonwebtoken');
const Web3 = require('web3');
const web3 = new Web3(Web3.givenProvider || 'http://localhost:8545');
const {Op} = require('sequelize');

async function verify(ethereumAddress, signature) {
  try {
    const models = db.getModels();

    let user = await models.Verifier.findOne({where: {ethereumAddress: {[Op.iLike]: ethereumAddress}}});
    if (!user) user = await models.BoardMember.findOne({where: {ethereumAddress: {[Op.iLike]: ethereumAddress}}});
    if (!user) user = await models.Manager.findOne({where: {ethereumAddress: {[Op.iLike]: ethereumAddress}}});
    if (!user) throw new AppError({status: 400, message: errorMessages.USER_NOT_FOUND});

    const userMessages = await models.UserMessage.findAll({
      where: {
        userId: user.id,
        userRole: user.role,
        status: 'NOT_ACTIVATED'
      }
    });
    if (!userMessages.length) throw new AppError({status: 400, message: errorMessages.USER_MESSAGE_NOT_FOUND});

    let verify = false;
    for (let i = 0; i < userMessages.length; i++) {
      if (Date.now() - new Date(userMessages[i].createdAt).getTime() > 600000) continue; //10 min
      const addr = web3.eth.accounts.recover(userMessages[i].message, signature);
      console.log('addr', addr)
      if (addr.toLowerCase() === user.ethereumAddress.toLowerCase()) {
        verify = true;
        userMessages[i].set('status', 'ACTIVATED');
        await userMessages[i].save();
        break;
      }
    }
    if (!verify) throw new AppError({status: 400, message: errorMessages.NOT_VERIFY});
    return {
      userId: user.id,
      userRole: user.role,
      firstName: user.firstName,
      lastName: user.lastName,
      accessToken: jwt.sign(user.toJSON(), config.server.secret, {expiresIn: config.server.expiresIn})
    };
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}


module.exports = verify;
