const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function getShareVcRequest(user) {
  try {
    const models = db.getModels();

    const verifiedCredentialDocuments = await models.VerifiedCredentialDocument.findAll({
      where: {
        userId:  user.id,
        userRole:  user.role,
      }
    });

    return await models.ShareVcRequest.findAll({
      where: {
        credentialId: verifiedCredentialDocuments.map(verifiedCredentialDocument => verifiedCredentialDocument.id),
        responseToken: null
      }
    });

  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = getShareVcRequest;
