const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function putShareVcRequest(user, body) {
  try {
    const models = db.getModels();

    let shareVcRequest = await models.ShareVcRequest.findOne({
      where: {
         id: body.shareVcRequestId
      }
    });
    if (!shareVcRequest) throw new AppError({status: 400, message: errorMessages.SHARE_VC_REQUEST_NOT_FOUND});

    shareVcRequest.set('responseToken', body.responseToken);
    return await shareVcRequest.save();
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = putShareVcRequest;
