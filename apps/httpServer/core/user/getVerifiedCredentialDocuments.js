const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function getVerifiedCredentialDocuments(user) {
  try {
    const models = db.getModels();
    return  await models.VerifiedCredentialDocument.findAll({
      where: {
        userId:  user.id,
        userRole: user.role,
      }
    });
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = getVerifiedCredentialDocuments;
