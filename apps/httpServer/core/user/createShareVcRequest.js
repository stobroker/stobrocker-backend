const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function createShareVcRequest(user, body) {
  try {
    const models = db.getModels();
    const proposal = await models.Proposal.findOne({
      where: {
        id: body.proposalId,
        companyId: user.companyId
      }
    });
    if (!proposal) throw new AppError({status: 400, message: errorMessages.PROPOSAL_NOT_FOUND});

    body.requesterId = user.id;
    body.requesterRole = user.role;
    return await models.ShareVcRequest.create(body);

  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = createShareVcRequest;
