const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');
const bcrypt = require('bcryptjs');

async function createOrUpdateVC(user, body) {
  try {
    const models = db.getModels();

    let params = body;
    let result = [];
    params.userId = user.id;
    params.userRole = user.role;
    let documents = JSON.parse(body.documentsJSON);

    for (let i = 0; i < documents.length; i++) {
      params.documentJSON = documents[i];
      params.idDocument = documents[i].id;
      params.hashDocumentJSON = bcrypt.hashSync(documents[i].toString(), 10);

      let verifiedCredentialDocument = await models.VerifiedCredentialDocument.findOne({
        where: {
          idDocument: documents[i].id,
          userId: user.id,
          userRole: user.role,
        }
      });
      if (verifiedCredentialDocument) {
        verifiedCredentialDocument.set('documentJSON', params.documentJSON);
        verifiedCredentialDocument.set('hashDocumentJSON', params.hashDocumentJSON);
        result.push(await verifiedCredentialDocument.save());
      } else result.push(await models.VerifiedCredentialDocument.create(params));
    }
    return result;

  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = createOrUpdateVC;
