const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function getShareVcRequests(user, proposalId) {
  try {
    const models = db.getModels();
    return await models.ShareVcRequest.findAll({
      where: {
        proposalId: proposalId
      }
    });

  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = getShareVcRequests;
