const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function getProposal(user, proposalId) {
  try {
    const models = db.getModels();

    const proposal = await models.Proposal.findOne({
      where: {id: proposalId},
      include: [
        {
          model: models.Manager,
          where: {companyId: user.companyId},
          as: 'manager'
        },
        {model: models.VerifiedCredentialDocument},
        {
          model: models.AuthorizationDocument,
          as: 'authorizationDocument'
        },
        {
          model: models.AdditionalDocument,
          as: 'additionalDocument'
        },
        {
          model: models.Issue,
          as: 'issue'
        },
        {
          model: models.SignatureBoardMember,
          include: [
            {
              model: models.BoardMember,
              as: 'boardMember'
            }],
          as: 'signatureBoardMembers'
        },
      ]
    });

    if (!proposal) return {};
    let proposalObj = proposal.toJSON();
    let manager = {};
    manager.id = proposal.manager.id;
    manager.firstName = proposal.manager.firstName;
    manager.lastName = proposal.manager.lastName;
    manager.patronymic = proposal.manager.patronymic;
    manager.ethereumAddress = proposal.manager.ethereumAddress;
    manager.photo = proposal.manager.photo;
    proposalObj.manager = manager;
    let verifiedCredentialDocuments = proposal.VerifiedCredentialDocuments;
    let verifiedCredentialDocumentsObj = [];
    for (let i = 0; i < verifiedCredentialDocuments.length; i++) {
      let obj = {};
      obj.id = verifiedCredentialDocuments[i].id;
      obj.userId = verifiedCredentialDocuments[i].userId;
      if (verifiedCredentialDocuments[i].userRole === 'manager') user = await models.Manager.findOne({where: {id: verifiedCredentialDocuments[i].userId}});
      else if (verifiedCredentialDocuments[i].userRole === 'boardMember') user = await models.BoardMember.findOne({where: {id: verifiedCredentialDocuments[i].userId}});
      else if (verifiedCredentialDocuments[i].userRole === 'verifier') user = await models.Verifier.findOne({where: {id: verifiedCredentialDocuments[i].userId}});
      obj.user = user;
      obj.documentJSON = verifiedCredentialDocuments[i].documentJSON;
      obj.isVerified = verifiedCredentialDocuments[i].isVerified;
      verifiedCredentialDocumentsObj.push(obj);
    }
    proposalObj.verifiedCredentialDocuments = verifiedCredentialDocumentsObj;

    proposalObj.authorizationDocument = proposal.authorizationDocument;
    proposalObj.additionalDocument = proposal.additionalDocument;
    proposalObj.issue = proposal.issue;

    let signatureBoardMembersObj = [];
    let signatureBoardMembers = proposal.signatureBoardMembers;
    for (let i = 0; i < signatureBoardMembers.length; i++) {
      let obj = {};
      obj.signature = signatureBoardMembers[i].signature;
      obj.ethereumAddress = signatureBoardMembers[i].boardMember.ethereumAddress;
      obj.firstName = signatureBoardMembers[i].boardMember.firstName;
      obj.lastName = signatureBoardMembers[i].boardMember.lastName;
      signatureBoardMembersObj.push(obj);
    }
    proposalObj.signatureBoardMembers = signatureBoardMembersObj;

    return proposalObj;

  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = getProposal;
