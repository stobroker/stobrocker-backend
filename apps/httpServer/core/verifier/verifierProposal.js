const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');
const {Op} = require('sequelize');

async function verifierProposal(verifier, body) {
  try {
    const models = db.getModels();
    const proposal = await models.Proposal.findOne({
      where: {
        id: body.proposalId,
        companyId: verifier.companyId,
        verifier: false
      },
    });
    if (!proposal) throw new AppError({status: 400, message: errorMessages.PROPOSAL_NOT_FOUND});
    proposal.set('verifier', true);
    proposal.set('verifierId', verifier.id);
    return await proposal.save();
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = verifierProposal;
