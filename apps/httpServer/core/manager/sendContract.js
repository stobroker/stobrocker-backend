const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function sendContract(manager, body) {
  try {
    const models = db.getModels();
    let proposal = await models.Proposal.findOne({
      where: {
        id: body.proposalId,
        companyId: manager.companyId,
        status: 'SIGNED',
      },
      include: [{
        model: models.Issue,
        where: {
          id: body.issueId,
        },
        as: 'issue'
      }]
    });
    if (!proposal) throw new AppError({status: 400, message: errorMessages.PROPOSAL_NOT_FOUND});

    proposal.set('contractSignature', body.contractSignature);
    return await proposal.save();

  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = sendContract;
