const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function createProposal(manager, body, authDoc, additionalDoc) {
  try {
    const models = db.getModels();

    let params = body;
    params.managerId = manager.id;
    params.companyId = manager.companyId;
    const proposal = await models.Proposal.create(params);

    const authorizationDocument = await models.AuthorizationDocument.create({
      managerId: manager.id,
      proposalId: proposal.id,
      documentURL: authDoc.path,
      authDocSign: params.authDocSign
    });

    const additionalDocument = await models.AdditionalDocument.create({
      managerId: manager.id,
      proposalId: proposal.id,
      documentURL: additionalDoc.path,
      additionalDocSign: params.additionalDocSign
    });

    body.providedCredentials = JSON.parse(body.providedCredentials);

    for (let i = 0; i < body.providedCredentials.length; i++) {
      console.log('body.providedCredentials', body.providedCredentials[i]);
      let verifiedCredentialDocument = await models.VerifiedCredentialDocument.findOne({
        where:{
          id: body.providedCredentials[i],
          userId: manager.id,
          userRole: manager.role,
        }
      });
      if (verifiedCredentialDocument) await verifiedCredentialDocument.addProposal(proposal);
    }

    let result = proposal.toJSON();
    result.authorizationDocument = authorizationDocument.toJSON();
    result.additionalDocumentawait = additionalDocument.toJSON();
    return result;
  } catch
      (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = createProposal;
