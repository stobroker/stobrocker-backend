const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function signedProposal(boardMember, body) {
  try {
    const models = db.getModels();

    const proposal = await models.Proposal.findOne({
      where: {
        id: body.proposalId,
        companyId: boardMember.companyId
      },
    });
    if (!proposal) throw new AppError({status: 400, message: errorMessages.PROPOSAL_NOT_FOUND});

    const issue = await models.Issue.findOne({
      where: {
        id: body.issueId,
        companyId: boardMember.companyId,
        status: 'COMPLETED'
      },
    });
    if (!issue) throw new AppError({status: 400, message: errorMessages.ISSUE_NOT_FOUND});

    proposal.set('status', 'SIGNED');

    return await proposal.save();
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = signedProposal;
