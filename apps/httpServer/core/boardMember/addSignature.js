const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function addSignature(boardMember, body) {
  try {
    const models = db.getModels();

    const signatureBoardMember = await models.SignatureBoardMember.findOne({
      where: {
        boardMemberId: boardMember.id,
        proposalId: body.proposalId,
      },
      include: [{
        model: models.Proposal,
        where: {
          companyId: boardMember.companyId
        },
        as: 'proposal'
      }]
    });
    let params = body;
    params.boardMemberId = boardMember.id;
    if (signatureBoardMember) {
      signatureBoardMember.set('signature', body.signature);
      return signatureBoardMember.save();
    }
    return await models.SignatureBoardMember.create(body);
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = addSignature;
