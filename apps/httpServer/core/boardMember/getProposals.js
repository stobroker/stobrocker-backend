const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function getProposals(boardMember) {
  try {
    const models = db.getModels();

    let result = [];
    const proposals = await models.Proposal.findAll({
      include: [
        {
          model: models.Manager,
          where: {companyId: boardMember.companyId},
          as: 'manager'
        },
        {model: models.VerifiedCredentialDocument},
        {
          model: models.AuthorizationDocument,
          as: 'authorizationDocument'
        },
        {
          model: models.AdditionalDocument,
          as: 'additionalDocument'
        },
        {
          model: models.Issue,
          as: 'issue'
        },
        {
          model: models.SignatureBoardMember,
          include: [
            {
              model: models.BoardMember,
              as: 'boardMember'
            }],
          as: 'signatureBoardMembers'
        },
      ]
    });

    for (let i = 0; i < proposals.length; i++) {
      let proposalObj = proposals[i].toJSON();
      let manager = {};
      manager.id = proposals[i].manager.id;
      manager.firstName = proposals[i].manager.firstName;
      manager.lastName = proposals[i].manager.lastName;
      manager.patronymic = proposals[i].manager.patronymic;
      manager.ethereumAddress = proposals[i].manager.ethereumAddress;
      manager.photo = proposals[i].manager.photo;
      proposalObj.manager = manager;
      let verifiedCredentialDocuments = proposals[i].VerifiedCredentialDocuments;
      let verifiedCredentialDocumentsObj = [];
      for (let i = 0; i < verifiedCredentialDocuments.length; i++) {
        let obj = {};
        obj.id = verifiedCredentialDocuments[i].id;
        obj.userId = verifiedCredentialDocuments[i].userId;
        if (verifiedCredentialDocuments[i].userRole === 'manager') user = await models.Manager.findOne({where: {id: verifiedCredentialDocuments[i].userId}});
        else if (verifiedCredentialDocuments[i].userRole === 'boardMember') user = await models.BoardMember.findOne({where: {id: verifiedCredentialDocuments[i].userId}});
        else if (verifiedCredentialDocuments[i].userRole === 'verifier') user = await models.Verifier.findOne({where: {id: verifiedCredentialDocuments[i].userId}});
        obj.user = user;
        obj.documentJSON = verifiedCredentialDocuments[i].documentJSON;
        obj.isVerified = verifiedCredentialDocuments[i].isVerified;
        verifiedCredentialDocumentsObj.push(obj);
      }
      proposalObj.verifiedCredentialDocuments = verifiedCredentialDocumentsObj;

      proposalObj.authorizationDocument = proposals[i].authorizationDocument;
      proposalObj.additionalDocument = proposals[i].additionalDocument;
      proposalObj.issue = proposals[i].issue;

      let signatureBoardMembersObj = [];
      let signatureBoardMembers = proposals[i].signatureBoardMembers;
      for (let i = 0; i < signatureBoardMembers.length; i++) {
        let obj = {};
        obj.signature = signatureBoardMembers[i].signature;
        obj.ethereumAddress = signatureBoardMembers[i].boardMember.ethereumAddress;
        signatureBoardMembersObj.push(obj);
      }
      proposalObj.signatureBoardMembers = signatureBoardMembersObj;
      result.push(proposalObj);
    }

    return result;
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = getProposals;
