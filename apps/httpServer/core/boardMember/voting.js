const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');
const {Op} = require('sequelize');

async function voting(boardMember, body) {
  try {
    const models = db.getModels();
    const dateNow = new Date(Date.now());
    let issue = await models.Issue.findOne({
      where: {
        id: body.issueId,
        startDate: {[Op.lte]: dateNow},
        endDate: {[Op.gte]: dateNow},
        companyId: boardMember.companyId
      },
    });
    if (!issue) throw new AppError({status: 400, message: errorMessages.ISSUE_NOT_FOUND});
    let params = body;
    params.boardMemberId = boardMember.id;

    let boardMemberVoice = await models.BoardMemberVoice.findOne({
      where: {
        issueId: body.issueId,
        boardMemberId: boardMember.id
      }
    });
    if (boardMemberVoice) throw new AppError({status: 400, message: errorMessages.BOARD_MEMBER_VOICE_EXIST});
    const myVoice = await models.BoardMemberVoice.create(params);
    const countPossibleVoice = await models.BoardMember.count({where: {companyId: boardMember.companyId}});
    const allBoardMemberVoices = await models.BoardMemberVoice.count({where: {issueId: body.issueId}});
    if (countPossibleVoice === allBoardMemberVoices){
      issue.set('status', 'COMPLETED');
      await issue.save();
    }
    return myVoice;
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = voting;
