const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function createIssue(boardMember, body) {
  try {
    const models = db.getModels();

    const proposal = await models.Proposal.findOne({
      where: {
        id: body.proposalId,
        companyId: boardMember.companyId
      },
    });
    if (!proposal) throw new AppError({status: 400, message: errorMessages.PROPOSAL_NOT_FOUND});

    let params = body;
    params.boardMemberId = boardMember.id;
    params.companyId = boardMember.companyId;
    const dateNow = new Date(Date.now());

    if (dateNow > new Date(params.startDate) && dateNow < new Date(params.endDate)) params.status = 'LAUNCHED';
    if (dateNow > new Date(params.endDate)) params.status = 'COMPLETED';

    proposal.set('status', 'WITH_ISSUE');
    await proposal.save();

    return await models.Issue.create(params);
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = createIssue;
