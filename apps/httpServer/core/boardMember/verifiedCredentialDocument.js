const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function verifiedCredentialDocument(boardMember, body) {
  try {
    const models = db.getModels();
    const verifiedCredentialDocument = await models.VerifiedCredentialDocument.findOne({
      where: {id: body.credentialId},
      include: [
        {
          model: models.Proposal,
          where: {id: body.proposalId}
        }
      ]
    });
    if (!verifiedCredentialDocument) throw new AppError({status: 400, message: errorMessages.CREDENTIAL_NOT_FOUND});
    verifiedCredentialDocument.set('isVerified', true);
    return await verifiedCredentialDocument.save();
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = verifiedCredentialDocument;
