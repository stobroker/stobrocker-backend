const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function resultVoting(boardMember, issueId) {
  try {
    const models = db.getModels();

    let issue = await models.Issue.findOne({
      where: {
        id: issueId,
        companyId: boardMember.companyId
      },
    });
    if (!issue) throw new AppError({status: 400, message: errorMessages.ISSUE_NOT_FOUND});
    issue = issue.toJSON();

    let voted = false;
    const myVoice = await models.BoardMemberVoice.findOne({
      where: {
        boardMemberId: boardMember.id,
        issueId: issue.id
      },
    });
    if (myVoice) voted = true;

    let countPossibleVoice = await models.BoardMember.count({where: {companyId: boardMember.companyId}});

    let supportPercentNeed = issue.supportPercentNeed;
    let minimalApprovalPercentNeed = issue.minimalApprovalPercentNeed;

    let boardMemberVoices = await models.BoardMemberVoice.findAll({where: {issueId: issueId}});

    const countVoiceYes = boardMemberVoices.filter(boardMemberVoice => boardMemberVoice.voice === 'YES').length;
    const countVoiceNo = boardMemberVoices.filter(boardMemberVoice => boardMemberVoice.voice === 'NO').length;
    const countVoiceAbstain = boardMemberVoices.filter(boardMemberVoice => boardMemberVoice.voice === 'ABSTAIN').length ;

    return {
      status: issue.status,
      startDate: issue.startDate,
      endDate: issue.endDate,
      countVoiceYes: countVoiceYes,
      countVoiceNo: countVoiceNo,
      countVoiceAbstain: countVoiceAbstain,
      supportPercentNeed: supportPercentNeed,
      minimalApprovalPercentNeed: minimalApprovalPercentNeed,
      countPossibleVoice: countPossibleVoice,
      voted: voted
    };

  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = resultVoting;
