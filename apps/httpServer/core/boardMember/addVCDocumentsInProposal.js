const errorMessages = require('./../../../../commonServices/errorMessages');
const AppError = require('./../../../../commonServices/error');
const db = require('../../../../db');

async function addVCDocumentsInProposal(boardMember, body) {
  try {
    const models = db.getModels();
    let proposal = await models.Proposal.findOne({
      where: {
        id: body.proposalId,
        companyId: boardMember.companyId
      }
    });
    if (!proposal) throw new AppError({status: 400, message: errorMessages.PROPOSAL_NOT_FOUND});

    body.providedCredentials = JSON.parse(body.providedCredentials);
    console.log('body.providedCreden44444',body.providedCredentials);
    let result = [];
    for (let i = 0; i < body.providedCredentials.length; i++) {
      console.log('body.providedCredentials', body.providedCredentials[i]);
      let verifiedCredentialDocument = await models.VerifiedCredentialDocument.findOne({
        where:{
          id: body.providedCredentials[i],
          userId: boardMember.id,
          userRole: boardMember.role,
        }
      });
      if (verifiedCredentialDocument) {
        await verifiedCredentialDocument.addProposal(proposal);
        result.push(verifiedCredentialDocument);
      }
    }

    return result;
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({err: err});
  }
}

module.exports = addVCDocumentsInProposal;
