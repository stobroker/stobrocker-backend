const AppError = require('./../../../commonServices/error');
const errorMessages = require('./../../../commonServices/errorMessages');
const db = require('../../../db');

async function reqBoardMember(req, res, next) {
  try {
    if (!req.user) throw new AppError({status: 400, message: errorMessages.USER_NOT_FOUND});
    if (req.user.role !== 'boardMember') throw new AppError({status: 400, message: errorMessages.BOARD_MEMBER_NOT_FOUND});

    const models = db.getModels();
    const existBoardMember = await models.BoardMember.findOne({where: {id: req.user.id}});

    if (!existBoardMember) throw new AppError({status: 400, message: errorMessages.BOARD_MEMBER_NOT_FOUND});
    req.user = existBoardMember.toJSON();
    next();
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({status: 500, message: errorMessages.SERVER_ERROR, err: err});
  }
}

module.exports = async function (req, res, next) {
  try {
    await reqBoardMember(req, res, next)
  } catch (err) {
    res.status(err.status || 500).json(err)
  }
};
