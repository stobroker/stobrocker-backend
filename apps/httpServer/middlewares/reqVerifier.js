const AppError = require('./../../../commonServices/error');
const errorMessages = require('./../../../commonServices/errorMessages');
const db = require('../../../db');

async function reqVerifier(req, res, next) {
  try {
    if (!req.user) throw new AppError({status: 400, message: errorMessages.USER_NOT_FOUND});
    if (req.user.role !== 'verifier') throw new AppError({status: 400, message: errorMessages.VERIFIER_NOT_FOUND});

    const models = db.getModels();
    const existVerifier = await models.Verifier.findOne({where: {id: req.user.id}});

    if (!existVerifier) throw new AppError({status: 400, message: errorMessages.VERIFIER_NOT_FOUND});
    req.user = existVerifier.toJSON();
    next();
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({status: 500, message: errorMessages.SERVER_ERROR, err: err});
  }
}

module.exports = async function (req, res, next) {
  try {
    await reqVerifier(req, res, next)
  } catch (err) {
    res.status(err.status || 500).json(err)
  }
};
