const expressJwt = require('express-jwt');
const config = require('../../../config');
const errorMessages = require('./../../../commonServices/errorMessages.js');

module.exports = expressJwt({
  secret: config.server.secret, fail: (req, res) => {
    if (!req.headers.authorization) res.send(401, errorMessages.MISSING_AUTHORIZATION_HEADER);
    res.send(401);
  }, algorithms: ['HS256']
});
