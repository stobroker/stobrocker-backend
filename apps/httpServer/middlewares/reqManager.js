const AppError = require('./../../../commonServices/error');
const errorMessages = require('./../../../commonServices/errorMessages');
const db = require('../../../db');

async function reqManager(req, res, next) {
  try {
    if (!req.user) throw new AppError({status: 400, message: errorMessages.USER_NOT_FOUND});
    console.log('req.user',req.user)
    if (req.user.role !== 'manager') throw new AppError({status: 400, message: errorMessages.MANAGER_NOT_FOUND});

    const models = db.getModels();
    const existManager = await models.Manager.findOne({where: {id: req.user.id}});

    if (!existManager) throw new AppError({status: 400, message: errorMessages.MANAGER_NOT_FOUND});
    req.user = existManager.toJSON();
    next();
  } catch (err) {
    if (err instanceof AppError) throw err;
    throw new AppError({status: 500, message: errorMessages.SERVER_ERROR, err: err});
  }
}

module.exports = async function (req, res, next) {
  try {
    await reqManager(req, res, next)
  } catch (err) {
    res.status(err.status || 500).json(err)
  }
};
