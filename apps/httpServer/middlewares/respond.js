'use strict';
const logger = require('../../../commonServices/logger');

async function respond(res, status, promise) {
  try {
    logger.verbose(logger.formatResponseInfo(res.req, status, promise));

    return await res.status(status).json(await promise);
  } catch (err) {
    res.status(err.status || 500).json(err);
  }
}

module.exports = respond;