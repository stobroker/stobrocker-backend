module.exports = function (sequelize, DataTypes) {

  const additionalDocument = sequelize.define('AdditionalDocument', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    managerId: {type: DataTypes.BIGINT, allowNull: false},
    proposalId: {type: DataTypes.BIGINT, allowNull: false},
    documentURL: {type: DataTypes.STRING, allowNull: true},
    additionalDocSign: {type: DataTypes.STRING, allowNull: true},
  }, {
    tableName: 'additionalDocuments'
  });

  additionalDocument.associate = function (models) {
    additionalDocument.belongsTo(models.Manager, {foreignKey: 'managerId', onDelete: 'CASCADE'});
    additionalDocument.belongsTo(models.Proposal, {foreignKey: 'proposalId', onDelete: 'CASCADE'});
  };

  additionalDocument.prototype.toJSON = function () {
    return {
      id: this.id,
      managerId: this.managerId,
      proposalId: this.proposalId,
      documentURL: this.documentURL,
      additionalDocSign: this.additionalDocSign
    };
  };

  return additionalDocument;
};
