module.exports = function (sequelize, DataTypes) {

  const Issue = sequelize.define('Issue', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    companyId: {type: DataTypes.BIGINT, allowNull: false},
    boardMemberId: {type: DataTypes.BIGINT, allowNull: false},
    proposalId: {type: DataTypes.BIGINT, allowNull: false},
    verifierId: {type: DataTypes.BIGINT, allowNull: true},
    title: {type: DataTypes.STRING, allowNull: true},
    description: {type: DataTypes.STRING(2055), allowNull: true},
    startDate: {type: DataTypes.DATE, allowNull: false},
    endDate: {type: DataTypes.DATE, allowNull: false,},
    supportPercentNeed: {type: DataTypes.FLOAT, allowNull: false},
    minimalApprovalPercentNeed: {type: DataTypes.FLOAT, allowNull: false},
    encryptionKey: {type: DataTypes.STRING, allowNull: true},
    status: {
      type: DataTypes.STRING(DataTypes.ENUM(['PENDING', 'LAUNCHED', 'COMPLETED'])),
      defaultValue: 'PENDING'
    },
  }, {
    paranoid: true,
    tableName: 'issues'
  });


  Issue.associate = function (models) {
    Issue.belongsTo(models.Verifier, {foreignKey: 'verifierId', as: 'verifier'});
    Issue.belongsTo(models.Proposal, {foreignKey: 'proposalId', as: 'proposal'});
    Issue.belongsTo(models.Company, {foreignKey: 'companyId', as: 'company'});
    Issue.hasMany(models.BoardMemberVoice, {foreignKey: 'issueId', as: 'BoardMemberVoices'});
  };

  Issue.prototype.toJSON = function () {
    const dateNow = new Date(Date.now());
    if (dateNow > new Date(this.startDate) && dateNow < new Date(this.endDate) && this.status !== 'COMPLETED') this.status = 'LAUNCHED';
    if (dateNow < new Date(this.startDate && this.status !== 'COMPLETED')) this.status = 'PENDING';
    if (dateNow > new Date(this.endDate)) this.status = 'COMPLETED';

    return {
      id: this.id,
      companyId: this.companyId,
      boardMemberId: this.boardMemberId,
      proposalId: this.proposalId,
      title: this.title,
      description: this.description,
      startDate: this.startDate,
      endDate: this.endDate,
      supportPercentNeed: this.supportPercentNeed,
      minimalApprovalPercentNeed: this.minimalApprovalPercentNeed,
      encryptionKey: this.encryptionKey,
      verifierId: this.verifierId,
      verifier: this.verifier,
      userVotes: this.userVotes,
      status: this.status,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    };
  };

  return Issue;
};
