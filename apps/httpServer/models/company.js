module.exports = function (sequelize, DataTypes) {

  const Company = sequelize.define('Company', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    name: {type: DataTypes.STRING, allowNull: true},
    address: {type: DataTypes.STRING, allowNull: true},
    photo: {type: DataTypes.STRING, allowNull: true},
  }, {
    paranoid: true,
    tableName: 'companies'
  });

  Company.associate = function (models) {
    Company.hasMany(models.Manager, {foreignKey: 'companyId', as: 'managers'});
    Company.hasMany(models.BoardMember, {foreignKey: 'companyId', as: 'boardMembers'});
    Company.hasMany(models.Verifier, {foreignKey: 'companyId', as: 'verifiers'});
    Company.hasMany(models.Proposal, {foreignKey: 'companyId', as: 'proposals'});
    Company.hasMany(models.Issue, {foreignKey: 'companyId', as: 'issues'});
  };

  Company.prototype.toJSON = function () {
    return {
      id: Number(this.id),
      name: this.name,
      address: this.address,
      photo: this.photo,
      managers: this.managers,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    }
  };

  return Company;
};
