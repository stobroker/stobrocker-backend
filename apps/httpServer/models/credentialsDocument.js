module.exports = function (sequelize, DataTypes) {

  const CredentialsDocument = sequelize.define('CredentialsDocument', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    managerId: {type: DataTypes.BIGINT, allowNull: false},
    proposalId: {type: DataTypes.BIGINT, allowNull: false},
    documentURL: {type: DataTypes.STRING, allowNull: false},
    name: {type: DataTypes.STRING, allowNull: true}, // 'credentialId', 'credentialEmployment'
  }, {
    tableName: 'credentialsDocuments'
  });

  CredentialsDocument.associate = function (models) {
    CredentialsDocument.belongsTo(models.Manager, {foreignKey: 'managerId', onDelete: 'CASCADE'});
    CredentialsDocument.belongsTo(models.Proposal, {foreignKey: 'proposalId', onDelete: 'CASCADE'});
  };

  CredentialsDocument.prototype.toJSON = function () {
    return {
      id: this.id,
      managerId: this.managerId,
      proposalId: this.proposalId,
      documentURL: this.documentURL,
      name: this.name
    };
  };

  return CredentialsDocument;
};
