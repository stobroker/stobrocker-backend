module.exports = function (sequelize, DataTypes) {

  const authorizationDocument = sequelize.define('AuthorizationDocument', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    managerId: {type: DataTypes.BIGINT, allowNull: false},
    proposalId: {type: DataTypes.BIGINT, allowNull: false},
    documentURL: {type: DataTypes.STRING, allowNull: true},
    authDocSign: {type: DataTypes.STRING, allowNull: true},
  }, {
    tableName: 'authorizationDocuments'
  });

  authorizationDocument.associate = function (models) {
    authorizationDocument.belongsTo(models.Manager, {foreignKey: 'managerId', onDelete: 'CASCADE'});
    authorizationDocument.belongsTo(models.Proposal, {foreignKey: 'proposalId', onDelete: 'CASCADE'});
  };

  authorizationDocument.prototype.toJSON = function () {
    return {
      id: this.id,
      managerId: this.managerId,
      proposalId: this.proposalId,
      documentURL: this.documentURL,
      authDocSign: this.authDocSign
    };
  };

  return authorizationDocument;
};
