module.exports = function (sequelize, DataTypes) {

  const SignatureBoardMember = sequelize.define('SignatureBoardMember', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    boardMemberId: {type: DataTypes.BIGINT, allowNull: false},
    proposalId: {type: DataTypes.BIGINT, allowNull: false},
    signature: {type: DataTypes.TEXT, allowNull: false},
  }, {
    tableName: 'signatureBoardMembers'
  });


  SignatureBoardMember.associate = function (models) {
    SignatureBoardMember.belongsTo(models.BoardMember, {foreignKey: 'boardMemberId', onDelete: 'CASCADE', as: 'boardMember'});
    SignatureBoardMember.belongsTo(models.Proposal, {foreignKey: 'proposalId', onDelete: 'CASCADE', as: 'proposal'});
  };

  SignatureBoardMember.prototype.toJSON = function () {
    return {
      id: this.id,
      proposalId: this.proposalId,
      boardMemberId: this.boardMemberId,
      signature: this.signature,
    };
  };

  return SignatureBoardMember;
};
