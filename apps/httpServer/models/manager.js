module.exports = function (sequelize, DataTypes) {
  const bcrypt = require('bcryptjs');

  const Manager = sequelize.define('Manager', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    companyId: {type: DataTypes.BIGINT, allowNull: false},
    email: {type: DataTypes.STRING, allowNull: true},
    password: {type: DataTypes.STRING, allowNull: true},
    firstName: {type: DataTypes.STRING, allowNull: true},
    lastName: {type: DataTypes.STRING, allowNull: true},
    patronymic: {type: DataTypes.STRING, allowNull: true},
    phone: {type: DataTypes.STRING, allowNull: true},
    ethereumAddress: {type: DataTypes.STRING, allowNull: true, unique: true},
    physicalAddress: {type: DataTypes.STRING, allowNull: true},
    dateOfBirth: {type: DataTypes.DATE, allowNull: true},
    role: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING), get: () => {
        return 'manager';
      }
    }
  }, {
    paranoid: true,
    tableName: 'managers'
  });


  Manager.beforeCreate((model, options) => {
    model.hashPassword();
  });

  Manager.prototype.hashPassword = function () {
    this.password = bcrypt.hashSync(this.password, 10);
  };

  Manager.associate = function (models) {
    Manager.belongsTo(models.Company, {foreignKey: 'companyId', onDelete: 'CASCADE'});
    Manager.hasMany(models.AuthorizationDocument, {foreignKey: 'managerId', as: 'authorizationDocuments'});
    Manager.hasMany(models.Proposal, {foreignKey: 'proposalId', as: 'proposals'});
  };

  Manager.prototype.toJSON = function () {
    return {
      id: this.id,
      companyId: this.companyId,
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName,
      patronymic: this.patronymic,
      phone: this.phone,
      ethereumAddress: this.ethereumAddress,
      role: this.role,
      physicalAddress: this.physicalAddress,
      dateOfBirth: this.dateOfBirth,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    };
  };

  return Manager;
};
