module.exports = function (sequelize, DataTypes) {

  const BoardMemberVoice = sequelize.define('BoardMemberVoice', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    boardMemberId: {type: DataTypes.BIGINT, allowNull: false},
    issueId: {type: DataTypes.BIGINT, allowNull: false},
    voice: {
      type: DataTypes.STRING(DataTypes.ENUM({values: ['YES', 'NO', 'ABSTAIN']})),
      allowNull: false
    },
  }, {
    paranoid: true,
    tableName: 'boardMemberVoices'
  });


  BoardMemberVoice.associate = function (models) {
    BoardMemberVoice.belongsTo(models.BoardMember, {foreignKey: 'boardMemberId', onDelete: 'Cascade', as: 'boardMember'});
    BoardMemberVoice.belongsTo(models.Issue, {foreignKey: 'issueId', onDelete: 'Cascade', as: 'issue'});
  };

  BoardMemberVoice.prototype.toJSON = function () {
    return {
      id: this.id,
      boardMemberId: this.boardMemberId,
      issueId: this.issueId,
      voice: this.voice,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    };
  };

  return BoardMemberVoice;
};
