module.exports = function (sequelize, DataTypes) {

  const VerifiedCredentialDocument = sequelize.define('VerifiedCredentialDocument', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    idDocument: {type: DataTypes.STRING, allowNull: false, unique: true},
    userId: {type: DataTypes.BIGINT, allowNull: false},
    userRole: {type: DataTypes.STRING, allowNull: false},
    documentJSON: {type: DataTypes.JSON, allowNull: false},
    hashDocumentJSON: {type: DataTypes.STRING, allowNull: true},
    isVerified: {type: DataTypes.BOOLEAN, defaultValue: false},
  }, {
    tableName: 'verifiedCredentialDocuments'
  });

  VerifiedCredentialDocument.associate = function (models) {

    VerifiedCredentialDocument.belongsToMany(models.Proposal, {through: models.ProposalVCDocumentLink});
  };

  VerifiedCredentialDocument.prototype.toJSON = function () {
    return {
      id: this.id,
      idDocument: this.idDocument,
      userId: this.userId,
      userRole: this.userRole,
      documentJSON: this.documentJSON,
      isVerified: this.isVerified,
    };
  };

  return VerifiedCredentialDocument;
};
