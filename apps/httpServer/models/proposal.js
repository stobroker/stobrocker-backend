module.exports = function (sequelize, DataTypes) {

  const Proposal = sequelize.define('Proposal', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    companyId: {type: DataTypes.BIGINT, allowNull: false},
    managerId: {type: DataTypes.BIGINT, allowNull: false},
    type: {
      type: DataTypes.STRING(DataTypes.ENUM({
        values: [
          'PURCHASE_CONTRACT_PROXY',
          'BANK_ACCOUNT_OPENING_PROXY',
          'BROKER_SERVICE_PROXY'
        ]
      })),
      allowNull: false
    },
    validity: {type: DataTypes.STRING, allowNull: true},
    description: {type: DataTypes.TEXT, allowNull: true},
    status: {type: DataTypes.STRING(DataTypes.ENUM({values: ['FOR_REVIEW', 'WITH_ISSUE', 'SIGNED']})), defaultValue: 'FOR_REVIEW'},
    verifier: {type: DataTypes.BOOLEAN, defaultValue: false},
    verifierId: {type: DataTypes.BIGINT, allowNull: true},
    contractSignature: {type: DataTypes.TEXT, allowNull: true},
  }, {
    tableName: 'proposals'
  });


  Proposal.associate = function (models) {
    Proposal.belongsTo(models.Manager, {foreignKey: 'managerId', onDelete: 'CASCADE', as: 'manager'});
    Proposal.belongsTo(models.Company, {foreignKey: 'companyId', onDelete: 'CASCADE', as: 'company'});
    Proposal.belongsTo(models.Verifier, {foreignKey: 'verifierId', onDelete: 'CASCADE'});
    Proposal.hasOne(models.Issue, {foreignKey: 'proposalId', as: 'issue'});
    Proposal.hasOne(models.AuthorizationDocument, {foreignKey: 'proposalId', as: 'authorizationDocument'});
    Proposal.hasOne(models.AdditionalDocument, {foreignKey: 'proposalId', as: 'additionalDocument'});
    Proposal.hasMany(models.ShareVcRequest, {foreignKey: 'proposalId', as: 'shareVcRequests'});
    Proposal.hasMany(models.SignatureBoardMember, {foreignKey: 'proposalId', as: 'signatureBoardMembers'});
    Proposal.belongsToMany(models.VerifiedCredentialDocument, {through: models.ProposalVCDocumentLink});
  };

  Proposal.prototype.toJSON = function () {
    return {
      id: this.id,
      managerId: this.managerId,
      type: this.type,
      status: this.status,
      verifier: this.verifier,
      verifierId: this.verifierId,
      description: this.description,
      validity: this.validity,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    };
  };

  return Proposal;
};
