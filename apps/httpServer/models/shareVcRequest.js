module.exports = function (sequelize, DataTypes) {

  const ShareVcRequest = sequelize.define('ShareVcRequest', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    credentialId: {type: DataTypes.BIGINT, allowNull: false},
    proposalId: {type: DataTypes.BIGINT, allowNull: false},
    requesterId: {type: DataTypes.BIGINT, allowNull: false},
    requesterRole: {type: DataTypes.STRING, allowNull: false},
    requestToken: {type: DataTypes.TEXT, allowNull: true},
    responseToken: {type: DataTypes.TEXT, allowNull: true},

  }, {
    tableName: 'shareVcRequests'
  });

  ShareVcRequest.associate = function (models) {
    ShareVcRequest.belongsTo(models.Proposal, {foreignKey: 'proposalId', onDelete: 'CASCADE'});
  };

  ShareVcRequest.prototype.toJSON = function () {
    return {
      id: this.id,
      managerId: this.managerId,
      proposalId: this.proposalId,
      credentialId: this.credentialId,
      requesterId: this.requesterId,
      requesterRole: this.requesterRole,
      requestToken: this.requestToken,
      responseToken: this.responseToken,
    };
  };

  return ShareVcRequest;
};
