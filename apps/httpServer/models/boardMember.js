module.exports = function (sequelize, DataTypes) {
  const bcrypt = require('bcryptjs');

  const BoardMember = sequelize.define('BoardMember', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    companyId: {type: DataTypes.BIGINT, allowNull: false},
    email: {type: DataTypes.STRING, allowNull: true},
    password: {type: DataTypes.STRING, allowNull: true},
    firstName: {type: DataTypes.STRING, allowNull: true},
    lastName: {type: DataTypes.STRING, allowNull: true},
    patronymic: {type: DataTypes.STRING, allowNull: true},
    phone: {type: DataTypes.STRING, allowNull: true},
    ethereumAddress: {type: DataTypes.STRING, allowNull: true},
    physicalAddress: {type: DataTypes.STRING, allowNull: true},
    dateOfBirth: {type: DataTypes.DATE, allowNull: true},
    role: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING), get: () => {
        return 'boardMember';
      }
    }
  }, {
    paranoid: true,
    tableName: 'boardMembers'
  });


  BoardMember.beforeCreate((model, options) => {
    model.hashPassword();
  });

  BoardMember.prototype.hashPassword = function () {
    this.password = bcrypt.hashSync(this.password, 10);
  };

  BoardMember.associate = function (models) {
    BoardMember.belongsTo(models.Company, {foreignKey: 'companyId', onDelete: 'CASCADE'});
    BoardMember.hasMany(models.SignatureBoardMember, {foreignKey: 'boardMemberId', as: 'signatureBoardMembers'});
  };

  BoardMember.prototype.toJSON = function () {
    return {
      id: this.id,
      companyId: this.companyId,
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName,
      patronymic: this.patronymic,
      phone: this.phone,
      ethereumAddress: this.ethereumAddress,
      role: this.role,
      physicalAddress: this.physicalAddress,
      dateOfBirth: this.dateOfBirth,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    };
  };

  return BoardMember;
};
