module.exports = function (sequelize, DataTypes) {

  const ProposalVCDocumentLink = sequelize.define('ProposalVCDocumentLink', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},

  }, {
    paranoid: false,
    tableName: 'proposalVCDocumentLink'
  });

  return ProposalVCDocumentLink;
};
