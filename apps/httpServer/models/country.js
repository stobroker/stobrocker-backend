module.exports = function (sequelize, DataTypes) {

  const Country = sequelize.define('Country', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    name: {type: DataTypes.STRING, allowNull: false},
    flagURL: {type: DataTypes.STRING, allowNull: false}
  }, {
    paranoid: true,
    tableName: 'countries'
  });

  Country.associate = function (models) {
    Country.hasMany(models.Manager, {foreignKey: 'companyId', as: 'managers'});
    Country.hasMany(models.BoardMember, {foreignKey: 'companyId', as: 'boardMembers'});
    Country.hasMany(models.Verifier, {foreignKey: 'companyId', as: 'verifiers'});
  };

  Country.prototype.toJSON = function () {
    return {
      id: this.id,
      name: this.name,
      flagURL: this.flagURL,
      managers: this.managers,
      boardMembers: this.boardMembers,
      verifiers: this.verifiers,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    };
  };

  return Country;
};
