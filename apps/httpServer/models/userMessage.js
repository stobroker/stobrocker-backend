module.exports = function (sequelize, DataTypes) {

    const UserMessage = sequelize.define('UserMessage', {
        id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
        userId: {type: DataTypes.BIGINT, allowNull: false},
        userRole: {type: DataTypes.STRING, allowNull: false},
        message: {type: DataTypes.TEXT, allowNull: false},
        status: {type: DataTypes.STRING, defaultValue: 'NOT_ACTIVATED'},
    }, {
        tableName: 'userMessages'
    });


    UserMessage.prototype.toJSON = function () {
        return {
            id: this.id,
            userId: this.userId,
            userRole: this.userRole,
            message: this.message
        };
    };

    return UserMessage;
};
