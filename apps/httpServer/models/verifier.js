module.exports = function (sequelize, DataTypes) {
  const bcrypt = require('bcryptjs');

  const Verifier = sequelize.define('Verifier', {
    id: {type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true},
    companyId: {type: DataTypes.BIGINT, allowNull: false},
    email: {type: DataTypes.STRING, allowNull: true},
    password: {type: DataTypes.STRING, allowNull: true},
    firstName: {type: DataTypes.STRING, allowNull: true},
    lastName: {type: DataTypes.STRING, allowNull: true},
    patronymic: {type: DataTypes.STRING, allowNull: true},
    phone: {type: DataTypes.STRING, allowNull: true},
    ethereumAddress: {type: DataTypes.STRING, allowNull: true, unique: true},
    physicalAddress: {type: DataTypes.STRING, allowNull: true},
    dateOfBirth: {type: DataTypes.DATE, allowNull: true},
    role: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING), get: () => {
        return 'verifier';
      }
    }
  }, {
    paranoid: true,
    tableName: 'verifiers'
  });


  Verifier.beforeCreate((model, options) => {
    model.hashPassword();
  });

  Verifier.prototype.hashPassword = function () {
    this.password = bcrypt.hashSync(this.password, 10);
  };

  Verifier.associate = function (models) {
    Verifier.belongsTo(models.Country, {foreignKey: 'countryId', onDelete: 'Cascade', as: 'country'});
    Verifier.belongsTo(models.Company, {foreignKey: 'companyId', onDelete: 'Cascade', as: 'company'});
    Verifier.hasMany(models.Issue, {foreignKey: 'verifierId', as: 'issues', onDelete: 'Cascade'});
    Verifier.hasMany(models.Proposal, {foreignKey: 'verifierId', as: 'proposals', onDelete: 'Cascade'});
  };

  Verifier.prototype.toJSON = function () {
    return {
      id: this.id,
      companyId: this.companyId,
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName,
      patronymic: this.patronymic,
      phone: this.phone,
      ethereumAddress: this.ethereumAddress,
      role: this.role,
      physicalAddress: this.physicalAddress,
      dateOfBirth: this.dateOfBirth,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    };
  };

  return Verifier;
};
