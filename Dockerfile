FROM node:14 AS modules

WORKDIR /app

COPY ./package*.json ./
RUN npm i

FROM modules AS app

WORKDIR /app

COPY . .

EXPOSE 3000

CMD ["npm","start"]